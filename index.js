function oddFunction() {
    let enteredNumbers = prompt("Enter your numbers");
    let oddNumbersCounter = 0;

    // checking if entered number is integer
    while (Number.parseInt(enteredNumbers) != enteredNumbers) {
        enteredNumbers = prompt("Enter your numbers", "Entered number isn't integer");
    }

    // forming array of numbers from 0 until entered
    let myArray = [];
    for (let i = 0; i <= enteredNumbers; i++) {
        myArray.push(i);
    }

    for (let i = 1; i < myArray.length; i++) {
        if (myArray[i] % 5 == 0) {
            oddNumbersCounter++;
        }
    }

    document.getElementById("result").innerHTML = ("Numbers in range: " + myArray);
    console.log("Numbers in range: " + myArray);

    if (oddNumbersCounter > 0) {
        document.getElementById("result2").innerHTML = ("Quantity of numbers x5 is: " + oddNumbersCounter);
        console.log("Quantity of numbers x5 is: "+ oddNumbersCounter);
    } else {
        document.getElementById("result2").innerHTML = ("Sorry, no numbers");
        console.log("Sorry, no numbers");
    }
}

function mnFunction() {
    let m = prompt("Enter 'M' number");
    let n = prompt("Enter 'N' number (Must be greater than previous number)");
    let resultArray = [];
    let primeArray = [];
    let mInt = Number.parseInt(m);

    //*** Ne zrozumiv umovi validatsii chisel, tomy zalishau tak ***

    // forming array of numbers from m until n
    for (let i = mInt; i <= n; i++) {
        resultArray.push(i);
    }

    for (let j = mInt; j <= n; j++) {
        let flag = 1;
        for (let k = 2; (k <= j / 2) && (flag == 1); k++) {
            if (j % k == 0) {
                flag = 0
            }
        }
        if (flag == 1) {
            primeArray.push(j);
        }
    }

    document.getElementById("result").innerHTML = ("Numbers in range: " + resultArray);
    document.getElementById("result2").innerHTML = ("PRIME digits is: " + primeArray);
    console.log("Numbers in range: " + resultArray);
    console.log("PRIME digits is: " + primeArray);
}