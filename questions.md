1. Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We need cycles for making a loop thru the arrays and objects, and on each
iteration we can perform different tasks, depending from our needs.
For example, we can cycle entering data from customer, until he will
enter exceptable parameters, such as name/login/password

2. Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Like I said, I used cycles to make a customer to enter the correct data, and
iterate thru some numbers to fill array

3. Що таке явне та неявне приведення (перетворення) типів даних у JS?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

неявне:
That means that JavaScript can automatically convert different types of data.
Example:
let a = 3;
let b = "25";
let c = a = b;
console.log(c);

явне:
We need to implement manual casting of data. Example:
let newInt = Number.parseint(somestring);